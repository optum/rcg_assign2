##React, the Complete Guide - Udemy Course
###Assignment 2
S. Davis - 09/03/2018

#####Added ("Extra Credit") Features:

1.  Set a maximum length value on the input element (maxLength attribute).
2.  Added user instructions that give the user guidance on the minimum and maximum text length to enter.
3.  The minimum and maximum values are not hard coded.  I pass them to the ValicationComponent, I use them in the user instructions, and to set the maxLength imput attribute.
4.  Leading whitespace is not allowed in the text box. Also, if you enter whitespace in the middle of the input (allowed) but then delete the characters in fromt of the space, the whitespaces (all of them) will be trimmed.
5.  The ValicationComponent responds not only to 'text too short' and 'text long enough', it also give feedback for zero length text ('enter some text') and 'max text entered'.

#####Questions for this Assignment
Q:  What did you find most challenging and how did you overcome the challenge?

A:  I guess the biggest challenge for me is trying to figure out the best acceptable pattern for a given coding task.
For Example, here are a few things that I had to stop and think about:

1.  What is the best way to pass initialization parameters to a component that is used in the app component? For example, I did not want to hard code the minimum an maximum values for the text length.  So, I created an obvious data object in the app component called validationParams which contains the values for the minimum and maximum text values. Then, I passed these values to the ValidationComponent via props. This approach seems reasonable.  I also wanted to use these values for the user instructions (i.e., "Please enter some text (minimum X characters, maximum Y characters)") and the maximum value for the input element itself (for the maxLength attribute).

2.  I was a bit confused about the 'value' that is passed from the input element to my onTextChangeHandler(). It seems that auto attributes are available on the target object, like value, but I couldn't find any documentation on which attributes are available per form element. Are there other attributes that are automagically available?

3.  I stored both the text input and the text input length in the state.  It seems redundant to store the text length since the length is part of the string's attributes.  I felt a bit lazy when I added the length to the state.

4.  I'm not that good at creating advanced styles from scratch.  For past projects in other frameworks, I have used Bootstrap, Foundation, and others.  I guess CSS is for another course...but if anyone has any suggestions or what tools and libraries others use (?)  There seems to be so many choices out there.  I just found Sierra (https://sierra-library.github.io/) the other day (for example).  It looks nice.