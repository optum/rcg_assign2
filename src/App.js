import React, { Component } from 'react';

import './App.css';

// Components
import ValidationComponent  from './components/ValidationComponent.js';
import CharComponent        from './components/CharComponent.js';

class App extends Component {

    //============================================================================================
    // COMPONENT STATE
    //============================================================================================
    // App component state
    state = {
        text: "",           // The input text
        textLength: 0,       // Input text length
        value: ""
    };

    //============================================================================================
    // OTHER DATA
    //============================================================================================
    // Text Validation Parameters:
    // These parameters are used in the user instructions AND passed to the validation component.
    // The maximum lenght is also used in the input tag as maxLength
    validationParams = {
        minTextLength: 5,   //<=== The minimum text length
        maxTextLength: 20   //<=== The maximum text length (also used
    };

    //============================================================================================
    // UTILITY FUNCTIONS (these should come from lodash or some other utilities package)
    //============================================================================================

    // Left Trim White Spaces:
    // Trims all white space from the beginning of a given string
    leftTrim = (s) => {
        if(s == null) return s;
        return s.replace(/^\s+/g, '');
    };

    //============================================================================================
    // EVENT HANDLERS
    //============================================================================================

    //--------------------------------------------------------------------------------------------
    // Text Input Change Event Handler:
    // Called when text input is changed (each character)
    onTextChangeHandler = (e) => {
        // Get the text from the input control
        let theText = (e.target.value);
        // Filter white spaces from beginning of the string
        theText = this.leftTrim(theText);
        // Update the state including the displayed text in the input field.
        this.setState({
            text: theText,
            textLength: (theText).length,
            value: theText
        });
    };

    //--------------------------------------------------------------------------------------------
    // Delete Character Handler:
    // Deletes the character that the user clicks on by taking the character out of the current
    // string in the state and updating the state.
    deleteCharacterHandler = (charIndex) => {
        console.log("DELETE EVENT with Index: " + charIndex);

        let theText = this.state.text;
        let modText = theText.slice(0, charIndex) + theText.slice(charIndex + 1, theText.length);
        modText = this.leftTrim(modText);
        console.log("Modified Text = " + modText);

        this.setState({
            text: modText,
            textLength: modText.length,
            value: modText
        });
    };

    //============================================================================================
    // RENDER
    //============================================================================================
    render() {

        // --------------------------------------------------------------------------------------
        // Conditional Character Content
        // Converts the input string in the state to box characters for display.

        let displayChars = null;
        if (this.state.textLength > 0) { // <=== The text length must be greater than 0
            displayChars = (
                <div>
                    {this.state.text.split("").map((theChar, index) => {
                        return <CharComponent
                            theChar={theChar}
                            key={index}
                            click={() => this.deleteCharacterHandler(index)}
                        />
                    })}
                </div>
            )
        }

        //---------------------------------------------------------------------------------------
        // RETURN
        return (
            <div className="App">
                <div className="App-header">
                    <h2>React, the Complete Guide - Assignment 2</h2>
                    <img src="/sdavis_avitar_head.png" height="30" width="30" alt="Me!"/>S. Davis -
                    03/09/2018
                </div>
                {/* User Text Input  ---------------------------------------------------------- */}
                <div>
                    <p>Please enter some text
                        (minimum {this.validationParams.minTextLength} characters,
                        maximum {this.validationParams.maxTextLength} characters)</p>
                    <p>
                        <input type="text" maxLength={this.validationParams.maxTextLength}
                               onChange={this.onTextChangeHandler}
                               value={this.state.value}/>
                    </p>
                    <p>Text Length: {this.state.textLength}</p>
                </div>
                {/* Text Validation  ---------------------------------------------------------- */}
                <div>
                    {<ValidationComponent
                        textLength={this.state.textLength}
                        valTextMinLength={this.validationParams.minTextLength}
                        valTextMaxLength={this.validationParams.maxTextLength}
                    />}
                </div>
                {/* The Characters  ----------------------------------------------------------- */}
                <div>
                    {displayChars}
                </div>
            </div>
        );
    }
}

export default App;
