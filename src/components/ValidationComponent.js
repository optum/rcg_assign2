/**
 * rcg_assign2_sdavis
 * Created by Steve Davis on 3/9/18.
 * (c) 2018, Steve Davis -- All Rights Reserved.
 */

// React
import React from 'react';
// Gives access to styles
import "./Component.css";

// The following properties are passed to this component:
//
//  Validation Parameters:
//      valTextMinLength:  The minimum length for the text.
//      valTextMaxLength:  The maximum length for the text.
//  The Text Length to Validate:
//      textLength:  The length to validate.
//
const validationcomponent = (props) => {

    // The message that will be returned in the rendered HTML
    let theMessage = "";

    // Output message choices
    const msg = {
        PLEASE_ENTER: "Please enter some text.",
        TOO_SHORT: "The text is too short.",
        LONG_ENOUGH: "The length is long enough.",
        MAX_REACHED: "You have reached the maximum text length.",
        ERROR: "Oops.  Error."
    };

    // Validation Function:
    // Determine the correct message for a given text length
    switch (true) {
        case (props.textLength === 0):
            theMessage = msg.PLEASE_ENTER;
            break;
        case ((props.textLength > 0) &&
              (props.textLength < props.valTextMinLength)):
            theMessage = msg.TOO_SHORT;
            break;
        case ((props.textLength >= props.valTextMinLength) &&
              (props.textLength < props.valTextMaxLength)):
            theMessage = msg.LONG_ENOUGH;
            break;
        case (props.textLength === props.valTextMaxLength):
            theMessage = msg.MAX_REACHED;
            break;
        default:
            theMessage = msg.ERROR;
    }

    return (
        <div className="ValidationComponent">
            { theMessage.length === 0 ? msg.PLEASE_ENTER : theMessage }
        </div>
    )
};

export default validationcomponent;