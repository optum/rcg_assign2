/**
 * rcg_assign2_sdavis
 * Created by Steve Davis on 3/9/18.
 * (c) 2018, Steve Davis -- All Rights Reserved.
 */

// React
import React from 'react';
// Gives access to styles
import "./Component.css";

// This component creates styled and clickable boxes to hold individual characters.
const charcomponent = (props) => {
    return (
        <div className="CharComponent" onClick={props.click}>
            {props.theChar}
        </div>
    )
};

export default charcomponent;